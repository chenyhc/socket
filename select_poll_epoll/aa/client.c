#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <arpa/inet.h>
#include <fcntl.h>

#define PORT 8989
#define QUIT_STR "quit"

int main(int argc, const char* argv[])
{
    //1.创建套接字
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd ==-1)
    {
	perror("socket error\n");
	exit(1);
    }

    //2.连接服务器
    struct sockaddr_in serv_addr;
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY); //ip地址

    int ret = connect(fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
    if (ret ==-1)
    {
	perror("connect error\n");
	exit(1);
    }

    while (1)
    {
	//写数据
	char buf[512] = {0};
	fgets(buf, sizeof(buf), stdin);
	//发送给服务器
	write(fd, buf, strlen(buf));
	if(!strncasecmp(buf, QUIT_STR,strlen(QUIT_STR)))
	{
	    printf("client is exiting\n");
	    break;
	}
    }

    close(fd);
    return 0;
}
